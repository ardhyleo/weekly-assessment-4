import "./style.css";
const Login = () => {
  return (
    <div className="Container2">
      <div className="account">
        <div className="input-wrapper">
          <div>
            <p className="Name">Name</p>
            <input id="textbox" type="text"></input>
          </div>
          <div>
            <p className="Email">Email</p>
            <input type="email"></input>
          </div>
        </div>
        <button id="login" className="btn-log">
          Sign me up
        </button>
      </div>
    </div>
  );
};

export default Login;
