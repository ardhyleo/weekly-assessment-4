import "./style.css";
const Text = () => {
  return (
    <div className="Container1">
      <div>
        <div className="text-wrapper">
          <h1>
            GOT MARKETING?<br></br> ADVANCE YOUR <br></br>BUSINESS INSIGHT.
          </h1>
          <p className="Text">
            Fill out the form and receive our <br></br>award winning newsletter.
          </p>
        </div>
      </div>
    </div>
  );
};
export default Text;
