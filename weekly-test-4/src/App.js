import "./App.css";
import Login from "./feature/Login";
import Text from "./feature/Text";

function App() {
  return (
    <div className="main-container">
      <Text />
      <Login />
    </div>
  );
}

export default App;
